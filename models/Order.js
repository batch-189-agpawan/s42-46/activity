const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({

	userId: {
		type : String,
		required: [true, "userId is required"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	orders: [
		{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			quantity: {
				type: Number,
			},
			subtotal: {
				type: Number
			}
		}
	]
});

module.exports = mongoose.model("Order", orderSchema);
