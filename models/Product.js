const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	category: {
		type: String,
		required:[true, "category is required"]
	},
	rarity: {
		type: String,
		required:[true, "rarity is required"]
	},
	stock: {
		type: Number,
		required:[true, "stock is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orderers: [
		{
			userId: {
				type: String,
				required: [true, "UserID is required"]
			},
			orderedOn: {
				type: Date,
				default: new Date()
			}
		}


	]


})


module.exports = mongoose.model("Product", productSchema);