const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');


//Create Product (Admin Only)
router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin){
	productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send('Unauthorized to add product')
	}

});

//Retrieve all active products
router.get("/", (req,res) =>{

	productController.getAllActive().then(resultFromController => res.send(resultFromController))
});

//Retrieve by category
router.get("/category", (req,res) =>{

	productController.productCategory().then(resultFromController => res.send(resultFromController))
});

//Get all products
router.get("/allproducts", (req,res) =>{

	productController.getAll().then(resultFromController => res.send(resultFromController))
});

//Retrieve specific product
router.get("/:productId", (req, res) =>{

	console.log(req.params)

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
});


//Update Product information (Admin only)
router.put("/:productId/update", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin){
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send('Unauthorized action')
	}

})


//archive product (Admin only)
router.put('/:productId/archive', auth.verify, (req, res) => {

	const data = {
		productId : req.params.productId,
		payload : auth.decode(req.headers.authorization).isAdmin
	}

	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController))
});




module.exports = router;
