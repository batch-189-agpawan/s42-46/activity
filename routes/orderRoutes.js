const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');
const Order = require('../models/Order');
const orderController = require('../controllers/orderController');

//make order

router.post("/order", auth.verify, (req, res) => {
	
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productId: req.body.productId
	}

	orderController.order(data).then(resultFromController => res.send(resultFromController))

});


// Retrieve all orders (Admin only)
router.get("/allorders", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    orderController.allOrders(userData).then(resultFromController => res.send(resultFromController))
})



// Get authenticated user order
router.get("/userorder", auth.verify, (req, res) => {
    
    const userData = auth.decode(req.headers.authorization);
    
     orderController.userOrder({id : userData.id}).then(resultFromController => res.send(resultFromController));
})







module.exports = router;