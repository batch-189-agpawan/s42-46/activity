const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');

//register user
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

//authenticate user
router.post("/login", (req, res) =>{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})


//set user as admin
router.put('/:userId/setadmin', auth.verify, (req, res) => {

	const data = {
		userId : req.params.userId,
		payload : auth.decode(req.headers.authorization).isAdmin
	}

	userController.setAdmin(data).then(resultFromController => res.send(resultFromController))
});


//Remove Admin rights
router.put('/:userId/removeAdmin', auth.verify, (req, res) => {

	const data = {
		userId : req.params.userId,
		payload : auth.decode(req.headers.authorization).isAdmin
	}

	userController.removeAdmin(data).then(resultFromController => res.send(resultFromController))
});



//Get User Details (Admin Only)
router.get("/:userId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {

		userController.getUser(req.params).then(resultFromController => res.send(resultFromController))

	} else {

		res.send('unauthorized action')
	}

});


//Get ALL Users
router.get("/userlist", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {

		userController.userList().then(resultFromController => res.send(resultFromController))

	} else {

		res.send('unauthorized action')
	}

});

//make order

router.post("/order", auth.verify, (req, res) => {
	
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productId: req.body.productId
	}

	userController.order(data).then(resultFromController => res.send(resultFromController))

});








module.exports = router;