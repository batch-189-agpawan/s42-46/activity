const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const orderRoutes = require('./routes/orderRoutes')
const port = 4001;

const app = express();


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);

mongoose.connect("mongodb+srv://kidcannabeast:admin123@zuitt-bootcamp.u0kf2.mongodb.net/s42-s46?retryWrites=true&w=majority", {
	useNewUrlParser:true,
	useUnifiedTopology:true
});

let db = mongoose.connection;

db.on('error', ()=> console.error.bind(console, 'error'));
db.once('open', ()=> console.log('Now connected to MongoDB Atlas!'));

app.listen(process.env.PORT || port, ()=> {
	console.log(`API is now online on port ${process.env.PORT || port}`)
})