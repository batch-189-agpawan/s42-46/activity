const Product = require('../models/Product');
const User = require('../models/User');

//Create a Product(Admin only)
module.exports.addProduct = (reqBody) => {

	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		category: reqBody.category,
		rarity: reqBody.rarity,
		stock: reqBody.stock
	});

	return newProduct.save().then((product, error) => {

		if(error) {

			return false

		} else {

			return 'Product has been Created!'

		}
	})

}


//Retrieve all active products
module.exports.getAllActive = () => {

	return Product.find({isActive: true}).then(result => {
		return result
	})
}

//Retrieve products by category
module.exports.productCategory = (reqBody) => {

	return Product.find(reqBody).then(result => {
		return result
	})
}

//Retrieve all products
module.exports.getAll = () => {

	return Product.find().then(result => {
		return result
	})
}


//Retrieve specific product
module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {
		return result
	})
} 

//Update Product information(Admin only)
module.exports.updateProduct = (reqParams, reqBody) => {

	let updatedProduct = {

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		category: reqBody.category,
		rarity: reqBody.rarity,
		stock: reqBody.stock
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) =>{

		if(error){
			return false
		} else {
			return product
		}
	})
}


//Archive Product
module.exports.archiveProduct = (data) => {

	return Product.findById(data.productId).then((result, error) => {

		if(data.payload === true) {

			result.isActive = false;

			return result.save().then((productArchived, error) => {

				if(error) {

					return false;

				} else {

					return 'product archived successfully';
				}
			})

		} else { 

			return "You must be an Admin to do that."
		}

	})
}

