const User = require('../models/User');
const bcrypt = require('bcrypt');
const Product = require('../models/Product');
const auth = require('../auth')

//Register User
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user,error) => {
		if(error){
			return false
		} else {
			return user
		}
	})
}

//User Authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null) {

			return false

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {

				return { access: auth.createAccessToken(result)}

			} else {
				
				return false
			}
		}
	})
}


//Set User to Admin
module.exports.setAdmin = (data) => {

	return User.findById(data.userId).then((result, error) => {

		if(data.payload === true) {

			result.isAdmin = true;

			return result.save().then((setToAdmin, error) => {

				if(error) {

					return false;

				} else {

					return 'User has been promoted to Admin';
				}
			})

		} else {

			return "You must be an Admin to do that."
		}

	})
}



//Remove Admin rights
module.exports.removeAdmin = (data) => {

	return User.findById(data.userId).then((result, error) => {

		if(data.payload === true) {

			result.isAdmin = false;

			return result.save().then((setToAdmin, error) => {

				if(error) {

					return false;

				} else {

					return true;
				}
			})

		} else {

			return "You must be an Admin to do that."
		}

	})
}


//Retrieve user details
module.exports.getUser = (reqParams) => {

	return User.findById(reqParams.userId).then(result => {

		return result
	})

}




