const User = require('../models/User');
const bcrypt = require('bcrypt');
const Product = require('../models/Product');
const auth = require('../auth')
const Order = require('../models/Order');



//Make order
// Non-admin User checkout (Create Order)  
module.exports.order = async (data) => {
if(data.isAdmin == true) {
        return "Admin not allowed to Order"
    } else {
        
    return User.findById(data.userId).then(result => {

            return Product.findById(data.productId).then(result => {

                    let isUserUpdated = User.findById(data.userId).then(user => {

                        user.orders.push({ productId: data.productId });

                        return user.save().then((user, error) => {

                            if (error) {
                                return false
                            } else {

                                return true
                            }

                        })

                    })

                    let isProductUpdated = Product.findById(data.productId).then(product => {

                        product.buyers.push({ userId: data.userId });

                        return product.save().then((product, error) => {

                            if (error) {
                                return false
                            } else {
                                return true
                            }
                        })

                    })

                    let isOrderUpdated = Product.findById(data.productId).then(product => {

                        let newOrder = new Order({
                            userId: data.userId,
                            productId: product._id,
                            price: product.price
                        })

                        return newOrder.save().then((user, error) => {

                            if (error) {
                                return false

                            } else {
                                return true
                            }
                        })

                    })

                    if (isUserUpdated && isProductUpdated) {

                        return 'Product Added to Cart'

                    } else {

                        return false

                    }

                    return 'Product added to cart'
                })
                .catch(error => {
                    return ("productId not found")
                })
        })
        .catch(error => {
            return ("userId not found")
        })

    }
}



// Retrieve all orders (Admin only)
module.exports.allOrders = async (data) => {

    if (data.isAdmin) {

        return User.find({}).then(result => {

            return result

        })
    } else {

        return 'You must be an admin to do that'
    }
}

//Get authenticated user order
module.exports.userOrder = (data) => {

    return User.findById(data.id).then((result, error) => {

        if (error) {
            return error
        }

        return result.orders;
    })
}

